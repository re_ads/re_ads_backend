require('dotenv').config();
const cookieParser = require('cookie-parser');
const express = require('express');
const cors = require('cors');
const api = require('./src/api');
const app = express();

app.use(cors());

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cookieParser());

app.use('/api/v1', api);

const PORT = process.env.PORT || 8080;
app.listen(PORT, (err) => {
  err
    ? console.log('server run error: ' + err)
    : console.log('server is up and listening on port ' + PORT);
});
