const en_US = {
  areNullOrUndefined: 'Are Null or Undefined',
  isNullOrUndefined: 'Is Null or Undefined',
  inValidEmail: 'Invalid Email Address',
  userDoesNotExist: 'User does not exist',
  incorrectPassword: 'Incorrect password',
  serverError: 'Internal Server Error',
  isNull: 'Is Null',
  forbiddenError: 'Forbidden',
  duplicateEmail: 'The email address is already in use by another account',
  inValidPhoneNumber: 'Invalid Phone Number',
  badRequest: 'Bad request',
  deleteUserSuccess: 'Delete user success',
  inValidPassword: 'Invalid Password',
  updateUserSuccess: 'Update user success',
  duplicateCategory: 'The category is already in user',
  updateCategorySuccess: 'Update category success',
  deleteCategorySuccess: 'Delete category success',
  deletePostSuccess: 'Delete post success',
  updatePostSuccess: 'Update post success',
};

module.exports = en_US;
