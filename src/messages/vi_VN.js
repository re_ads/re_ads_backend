const vi_VN = {
  areNullOrUndefined: 'Là Null hoặc Undefined',
  isNullOrUndefined: 'Là Null hoặc Undefined',
  inValidEmail: 'Địa chỉ email không hợp lệ',
  userDoesNotExist: 'Tài khoản không tồn tại',
  incorrectPassword: 'Mật khẩu không đúng',
  serverError: 'Lỗi máy chủ nội bộ',
  isNull: 'Là Null',
  forbiddenError: 'Bạn không có quyền truy cập',
  duplicateEmail: 'Địa chỉ email đã được sử dụng bởi một tài khoản khác',
  inValidPhoneNumber: 'Số điện thoại không hợp lệ',
  badRequest: 'Bad request',
  deleteUserSuccess: 'Xóa người dùng thành công',
  inValidPassword: 'Mật khẩu không hợp lệ',
  updateUserSuccess: 'Cập nhật người dùng thành công',
  duplicateCategory: 'Danh mục đã tồn tại',
  updateCategorySuccess: 'Cập nhật danh mục thành công',
  deleteCategorySuccess: 'Xóa danh mục thành công',
  deletePostSuccess: 'Xóa bài viết thành công',
  updatePostSuccess: 'Cập nhật bài viết thành công',
};

module.exports = vi_VN;
