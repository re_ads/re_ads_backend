const en_US = require('./en_US');
const vi_VN = require('./vi_VN');
const messages = {
  en: en_US,
  vn: vi_VN,
};

module.exports = { messages };
