const sql = require('./db');

class Category {
  constructor(
    id,
    title,
    slug,
    parentId = null,
    metaTitle = null,
    content = null
  ) {
    (this.id = id),
      (this.parentId = parentId),
      (this.title = title),
      (this.metaTitle = metaTitle),
      (this.slug = slug),
      (this.content = content);
  }

  /**
   * @param {Category} category
   * **/
  createCategory(category) {
    return new Promise((resolve, reject) => {
      sql.query('INSERT INTO category SET ?', [category], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  /**
   * @param {Category} category
   * @param {string} id
   * **/
  updateCategory(category, id) {
    return new Promise((resolve, reject) => {
      const keys = Object.keys(category);
      let dataUpdate = [];
      for (const key of keys) {
        const setQuery = `${key}='${category[key]}'`;
        dataUpdate.push(setQuery);
      }
      sql.query(
        `UPDATE category SET ${dataUpdate.join(',')} WHERE id = ?`,
        [id],
        (err, res) => {
          if (err) {
            reject(err);
          }
          if (!res.changedRows) {
            reject();
          }

          resolve(res);
        }
      );
    });
  }

  /**
   * @param {string} id
   * **/
  deleteCategory(id) {
    return new Promise((resolve, reject) => {
      sql.query('DELETE FROM category WHERE id = ?', [id], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  getCategories() {
    return new Promise((resolve, reject) => {
      sql.query('SELECT * FROM category', (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  getCategoriesByPostSlug(slug) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT category.title, category.metaTitle, category.slug FROM post INNER JOIN post_category ON post.id = post_category.postId INNER JOIN category ON post_category.categoryId = category.id WHERE post.slug = ?',
        [slug],
        (err, res) => {
          if (err) {
            reject(err);
          }
          resolve(res);
        }
      );
    });
  }
}

module.exports = Category;
