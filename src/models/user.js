const { LIMIT, OVER } = require('../consts/pagination.consts');
const sql = require('./db');

class User {
  constructor(
    id,
    firstName,
    lastName,
    email,
    mobile,
    password,
    registeredAt,
    lastLogin,
    intro,
    profile,
    passwordReminderToken,
    passwordReminderExpire,
    status = 1
  ) {
    (this.id = id),
      (this.firstName = firstName),
      (this.lastName = lastName),
      (this.email = email),
      (this.mobile = mobile),
      (this.password = password),
      (this.registeredAt = registeredAt),
      (this.lastLogin = lastLogin),
      (this.intro = intro),
      (this.profile = profile),
      (this.passwordReminderToken = passwordReminderToken),
      (this.passwordReminderExpire = passwordReminderExpire),
      (this.status = status);
  }

  /**
   * @param {string} email
   * **/
  getUserInfoByEmail(email) {
    return new Promise((resolve, reject) => {
      sql.query('SELECT * FROM user WHERE email = ?', [email], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res[0]);
      });
    });
  }

  /**
   * @param {string} email
   * **/
  getUserByEmail(email) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT firstName, lastName, email, mobile, intro, profile FROM user WHERE email = ?',
        [email],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res[0]);
        }
      );
    });
  }

  /**
   * @param {User} user
   * **/
  createUser(user) {
    return new Promise((resolve, reject) => {
      sql.query('INSERT INTO user SET ?', [user], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  /**
   * @param {number} over
   * @param {number} limit
   * @param {Object} sort
   * @param {Object} filters
   * @param {string} keyword
   * **/
  getUsers(over, limit, sort, filters, keyword) {
    let fullSearchQuery = '';
    let sortQuery = '';
    let filtersQuery = [];

    if (keyword && typeof keyword === 'string') {
      fullSearchQuery = `MATCH (email, lastName, firstName) AGAINST ('${keyword}' IN NATURAL LANGUAGE MODE)`;
    }
    if (sort && Object.keys(sort).length !== 0 && sort.constructor === Object) {
      sortQuery = `ORDER BY ${sort.name} ${sort.type}`;
    } else {
      sortQuery = `ORDER BY email asc`;
    }

    if (
      filters &&
      Object.keys(filters).length !== 0 &&
      filters.constructor === Object
    ) {
      const keys = Object.keys(filters);
      for (const key of keys) {
        const query = `${key}=${filters[key]}`;
        filtersQuery.push(query);
      }
    }

    if (fullSearchQuery) {
      filtersQuery.push(fullSearchQuery);
    }
    if (Array.isArray(filtersQuery) && filtersQuery.length) {
      filtersQuery.unshift('');
    }

    const query = `SELECT id, firstName, lastName, email, mobile, registeredAt, lastLogin, status FROM user WHERE id NOT IN ( SELECT id FROM (SELECT id FROM user ${sortQuery} LIMIT ${over}) as ids ) ${filtersQuery.join(
      ' AND '
    )} ${sortQuery} LIMIT ${limit}`;

    return new Promise((resolve, reject) => {
      sql.query(query, (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  /**
   * @param {string} id
   * **/
  getUserById(id) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT id, firstName, lastName, email, mobile, registeredAt, lastLogin, status FROM user WHERE id = ?',
        [id],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res[0]);
        }
      );
    });
  }

  /**
   * @param {Object} user
   * @param {string} id
   * **/
  updateUser(user, id) {
    return new Promise((resolve, reject) => {
      const keys = Object.keys(user);
      let dataUpdate = [];
      for (const key of keys) {
        const setQuery = `${key}='${user[key]}'`;
        dataUpdate.push(setQuery);
      }
      sql.query(
        `UPDATE user SET ${dataUpdate.join(',')} WHERE id = ?`,
        [id],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res);
        }
      );
    });
  }

  /**
   * @param {string} id
   * **/
  deleteUserById(id) {
    return new Promise((resolve, reject) => {
      sql.query('DELETE FROM user WHERE id = ?', [id], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  /**
   * @param {string} passwordReminderToken
   * **/
  getUserByPasswordReminderToken(passwordReminderToken) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT * FROM user WHERE passwordReminderToken = ?',
        [passwordReminderToken],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res[0]);
        }
      );
    });
  }

  getTotalUser() {
    return new Promise((resolve, reject) => {
      sql.query('SELECT COUNT(*) as total FROM user', (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res[0].total);
      });
    });
  }
}

module.exports = User;
