const sql = require('./db');

class Tag {
  constructor(id, name, slug) {
    (this.id = id), (this.name = name), (this.slug = slug);
  }

  /**
   * @param {Tag} tag
   * **/
  createTag(tag) {
    return new Promise((resolve, reject) => {
      sql.query('INSERT INTO tag SET ?', [tag], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  getTagsByPostSlug(slug) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT tag.name, tag.slug FROM post INNER JOIN post_tag ON post.id = post_tag.postId INNER JOIN tag ON post_tag.tagId = tag.id WHERE post.slug = ?',
        [slug],
        (err, res) => {
          if (err) {
            reject(err);
          }
          resolve(res);
        }
      );
    });
  }
}

module.exports = Tag;
