const sql = require('./db');

class Role {
  constructor(id, name) {
    (this.id = id), (this.name = name);
  }

  /**
   * @param {string} email
   * **/
  getRoleByUserEmail(email) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT role.id, role.name FROM user INNER JOIN user_role ON user.id = user_role.userId INNER JOIN role ON user_role.roleId = role.id WHERE user.email = ?',
        [email],
        (err, res) => {
          if (err) {
            reject(err);
          }
          resolve(res);
        }
      );
    });
  }

  /**
   * @param {string} name
   * **/
  getRoleByName(name) {
    return new Promise((resolve, reject) => {
      sql.query('SELECT * FROM role WHERE name = ?', [name], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res[0]);
      });
    });
  }

  /**
   * @param {string} id
   * **/
  getRoleByUserId(id) {
    return new Promise((resolve, reject) => {
      sql.query(
        'SELECT role.id, role.name FROM user INNER JOIN user_role ON user.id = user_role.userId INNER JOIN role ON user_role.roleId = role.id WHERE user.id = ?',
        [id],
        (err, res) => {
          if (err) {
            reject(err);
          }
          resolve(res);
        }
      );
    });
  }
}

module.exports = Role;
