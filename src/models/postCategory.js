const sql = require('./db');

class PostCategory {
  constructor(postId, categoryId) {
    (this.postId = postId), (this.categoryId = categoryId);
  }

  createPostCategory(postCategory) {
    return new Promise((resolve, reject) => {
      sql.query(
        'INSERT INTO post_category SET ?',
        [postCategory],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res);
        }
      );
    });
  }

  deletePostCategoryByPostId(postId) {
    return new Promise((resolve, reject) => {
      sql.query(
        'DELETE FROM post_category WHERE postId = ?',
        [postId],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res);
        }
      );
    });
  }
}

module.exports = PostCategory;
