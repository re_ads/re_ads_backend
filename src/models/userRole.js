const sql = require('./db');

class UserRole {
  constructor(userId, roleId) {
    (this.userId = userId), (this.roleId = roleId);
  }

  /**
   * @param {UserRole} userRole
   * **/
  createUserRole(userRole) {
    return new Promise((resolve, reject) => {
      sql.query('INSERT INTO user_role SET ?', [userRole], (err, res) => {
        if (err) {
          reject(err);
        }
        console.log('hello kitty', res);
        resolve(res);
      });
    });
  }

  /**
   * @param {string} id
   * **/
  deleteUserRoleByUserId(id) {
    return new Promise((resolve, reject) => {
      sql.query('DELETE FROM user_role WHERE userId = ?', [id], (err, res) => {
        if (err) {
          reject(err);
        }
        console.log('hello kitty', res);

        resolve(res);
      });
    });
  }
}

module.exports = UserRole;
