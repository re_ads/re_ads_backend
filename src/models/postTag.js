const sql = require('./db');

class PostTag {
  constructor(postId, tagId) {
    (this.postId = postId), (this.tagId = tagId);
  }

  createPostTag(postTag) {
    return new Promise((resolve, reject) => {
      sql.query('INSERT INTO post_tag SET ?', [postTag], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  deletePostTagByPostId(postId) {
    return new Promise((resolve, reject) => {
      sql.query(
        'DELETE FROM post_tag WHERE postId = ?',
        [postId],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res);
        }
      );
    });
  }
}

module.exports = PostTag;
