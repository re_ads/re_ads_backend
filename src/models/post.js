const sql = require('./db');

class Post {
  constructor(
    id,
    authorId,
    title,
    slug,
    createdAt,
    metaTitle = null,
    summary = null,
    content = null,
    mapLatitude = null,
    mapLongitude = null,
    published = 0,
    updatedAt = null,
    publishedAt = null,
    status = 0
  ) {
    (this.id = id),
      (this.authorId = authorId),
      (this.title = title),
      (this.metaTitle = metaTitle),
      (this.slug = slug),
      (this.summary = summary),
      (this.published = published),
      (this.createdAt = createdAt),
      (this.updatedAt = updatedAt),
      (this.publishedAt = publishedAt),
      (this.content = content),
      (this.status = status),
      (this.mapLatitude = mapLatitude),
      (this.mapLongitude = mapLongitude);
  }
  /**
   * @param {Post} post
   * **/
  createPost(post) {
    return new Promise((resolve, reject) => {
      sql.query('INSERT INTO post SET ?', [post], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  /**
   * @param {number} over
   * @param {number} limit
   * @param {Object} sort
   * @param {Object} filters
   * @param {string} keyword
   * **/
  getPosts(over, limit, sort, filters, keyword) {
    let fullSearchQuery = '';
    let sortQuery = '';
    let filtersQuery = [];

    if (keyword && typeof keyword === 'string') {
      fullSearchQuery = `MATCH (title) AGAINST ('${keyword}' IN NATURAL LANGUAGE MODE)`;
    }
    if (sort && Object.keys(sort).length !== 0 && sort.constructor === Object) {
      sortQuery = `ORDER BY ${sort.name} ${sort.type}`;
    }
    if (
      filters &&
      Object.keys(filters).length !== 0 &&
      filters.constructor === Object
    ) {
      const keys = Object.keys(filters);
      for (const key of keys) {
        const query = `${key}='${filters[key]}'`;
        filtersQuery.push(query);
      }
    }

    if (fullSearchQuery) {
      filtersQuery.push(fullSearchQuery);
    }
    if (Array.isArray(filtersQuery) && filtersQuery.length) {
      filtersQuery.unshift('');
    }

    const query = `SELECT * FROM post WHERE id NOT IN ( SELECT id FROM (SELECT id FROM user ${sortQuery} LIMIT ${over}) as ids ) ${filtersQuery.join(
      ' AND '
    )} ${sortQuery} LIMIT ${limit}`;

    return new Promise((resolve, reject) => {
      sql.query(query, (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }

  getPostBySlug(slug) {
    return new Promise((resolve, reject) => {
      sql.query('SELECT * FROM post WHERE slug = ?', [slug], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res[0]);
      });
    });
  }

  /**
   * @param {Object} post
   * @param {string} id
   * **/
  updatePost(post, id) {
    return new Promise((resolve, reject) => {
      const keys = Object.keys(post);
      let dataUpdate = [];
      for (const key of keys) {
        const setQuery = `${key}='${post[key]}'`;
        dataUpdate.push(setQuery);
      }
      sql.query(
        `UPDATE post SET ${dataUpdate.join(',')} WHERE id = ?`,
        [id],
        (err, res) => {
          if (err) {
            reject(err);
          }

          resolve(res);
        }
      );
    });
  }

  /**
   * @param {string} id
   * **/
  deletePostById(id) {
    return new Promise((resolve, reject) => {
      sql.query('DELETE FROM post WHERE id = ?', [id], (err, res) => {
        if (err) {
          reject(err);
        }

        resolve(res);
      });
    });
  }
}

module.exports = Post;
