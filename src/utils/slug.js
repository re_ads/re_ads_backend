const slugify = require('slugify');
const shortid = require('shortid');

const config = {
  replacement: '-',
  remove: /[*+~.()'"!:@]/g,
  lower: true,
  strict: false,
  locale: 'vi',
};

const convertToSlug = (target, unique = true) => {
  const text = unique ? target + '-' + shortid.generate() : target;

  return slugify(text, config);
};

module.exports = convertToSlug;
