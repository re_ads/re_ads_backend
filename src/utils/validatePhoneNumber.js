const regex = /^[\+][0-9]{11,}$/;

const validatePhoneNumber = (phoneNumber) => {
    if (typeof phoneNumber === 'string'){
        return regex.test(phoneNumber);
    }

    return false;
}

module.exports = validatePhoneNumber;