const crypto = require('crypto');

const generateReminderToken = () => {
  return crypto.randomBytes(20).toString('hex');
};

module.exports = generateReminderToken;
