const checkParamsIsEmpty = (object) => {
  const keys = Object.keys(object);
  let keyIsEmpty = [];
  for (const key of keys) {
    if (Array.isArray(object[key]) && !object[key].length) {
      keyIsEmpty.push(key);
    } else if (!object[key]) {
      keyIsEmpty.push(key);
    }
  }

  if (Array.isArray(keyIsEmpty) && keyIsEmpty.length) {
    return {
      isEmpty: true,
      params: keyIsEmpty,
    };
  }

  return {
    isEmpty: false,
    params: [],
  };
};

module.exports = checkParamsIsEmpty;
