const regex = /^(?=.*[!@#$%^&*()_=+?])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;

const validatePassword = (password) => {
  if (typeof password === 'string') {
    return regex.test(password);
  }

  return false;
};

module.exports = validatePassword;
