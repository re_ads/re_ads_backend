const generatePassword = () => {
  const lowerAlphabetCharset = 'abcdefghijklmnopqrstuvwxyz';
  const upperAlphabetCharset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const numberCharset = '0123456789';
  const symbolCharset = '!@#$%^&*()_=?';

  const lowerAlphabetLength = 10;
  const upperAlphabetLength = 10;
  const numberLength = 10;
  const symbolLength = 10;

  let lowerAlphabet = '';
  let upperAlphabet = '';
  let number = '';
  let symbol = '';

  for (let i = 0; i < lowerAlphabetLength; i++) {
    lowerAlphabet =
      lowerAlphabet +
      lowerAlphabetCharset.charAt(
        Math.floor(Math.random() * lowerAlphabetCharset.length)
      );
  }

  for (let i = 0; i < upperAlphabetLength; i++) {
    upperAlphabet =
      upperAlphabet +
      upperAlphabetCharset.charAt(
        Math.floor(Math.random() * upperAlphabetCharset.length)
      );
  }

  for (let i = 0; i < numberLength; i++) {
    number =
      number +
      numberCharset.charAt(Math.floor(Math.random() * numberCharset.length));
  }

  for (let i = 0; i < symbolLength; i++) {
    symbol =
      symbol +
      symbolCharset.charAt(Math.floor(Math.random() * symbolCharset.length));
  }

  let result = lowerAlphabet + upperAlphabet + number + symbol;
  result = shuffelWord(result);
  return result;
};

const shuffelWord = (word) => {
  var shuffledWord = '';
  word = word.split('');
  while (word.length > 0) {
    shuffledWord += word.splice((word.length * Math.random()) << 0, 1);
  }
  return shuffledWord;
};

generatePassword();

module.exports = generatePassword;
