const { validate } = require('uuid');

const regex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

const validateEmail = (email) => {
  if (typeof email === 'string') {
    return regex.test(email);
  }

  return false;
};

module.exports = validateEmail;
