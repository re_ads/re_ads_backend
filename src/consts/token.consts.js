const tokenConsts = {
    EXPIRE_ACCESS_TOKEN: '10000s',
    EXPIRE_REFRESH_TOKEN: '10000s'
}

module.exports = tokenConsts;