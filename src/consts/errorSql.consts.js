const errorSql = {
  ER_DUP_ENTRY: 'ER_DUP_ENTRY',
  ER_BAD_FIELD_ERROR: 'ER_BAD_FIELD_ERROR',
};
module.exports = errorSql;
