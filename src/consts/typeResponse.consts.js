const typeResponse = {
  USERS: 'users',
  CATEGORIES: 'categories',
  POSTS: 'posts',
};

module.exports = typeResponse;
