const compareTimeConsts = {
    EQUAL: 'equal',
    GREATER: 'greater',
    LESSER: 'lesser',
}

module.exports = compareTimeConsts;