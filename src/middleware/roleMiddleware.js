const RoleModel = require('../models/role');
const roleModel = new RoleModel();

const roleBasedAuthorization = (req, res, next) => {
  const { user } = req;
  if (!user) {
    return res.status(401).json({
      errors: [
        {
          status: 401,
          title: 'Unauthorized',
        },
      ],
    });
  }

  roleModel
    .getRoleByUserEmail(user.email)
    .then((roles) => {
      const rolesName = roles.map((role) => role.name);
      req.roles = rolesName;
      next();
    })
    .catch((err) => {
      return res.status(500).json({
        errors: [
          {
            status: 500,
            title: 'Internal Server Error',
          },
        ],
      });
    });
};

module.exports = roleBasedAuthorization;
