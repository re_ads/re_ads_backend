const jwt = require('jsonwebtoken');

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers['authorization'];
  const token = authHeader && authHeader.split(' ')[1];
  if (!token) {
    return res.status(401).json({
      errors: [
        {
          status: 401,
          title: 'Token not exists',
        },
      ],
    });
  }
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.status(403).json({
        errors: [
          {
            status: 403,
            title: 'Forbidden',
          },
        ],
      });
    }
    req.user = user;
    next();
  });
};

module.exports = authenticateToken;
