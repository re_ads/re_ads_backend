const express = require('express');
const authRoutes = require('./auth');
const adminRoutes = require('./admin_site');
const userRoutes = require('./user_site');

const router = express.Router();

router.use('/auth', authRoutes);
router.use('/admin', adminRoutes);
router.use('/', userRoutes);

module.exports = router;
