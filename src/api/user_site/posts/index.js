const express = require('express');
const postRoutes = express.Router();

const PostModel = require('../../../models/post');
const TagModel = require('../../../models/tag');
const CategoryModel = require('../../../models/category');
const { messages } = require('../../../messages');
const pagination = require('../../../consts/pagination.consts');
const errorSql = require('../../../consts/errorSql.consts');
const typeResponse = require('../../../consts/typeResponse.consts');
const postModel = new PostModel();
const tagModel = new TagModel();
const categoryModel = new CategoryModel();

postRoutes.get('/', (req, res) => {
  const { page, filter, sort, keyword } = req.query;
  let pageNumber = pagination.PAGE_NUMBER;
  let limit = pagination.LIMIT;

  if (page && Object.keys(page).length !== 0 && page.constructor === Object) {
    if (Object.prototype.hasOwnProperty.call(page, 'number')) {
      pageNumber = page.number;
    }
    if (Object.prototype.hasOwnProperty.call(page, 'size')) {
      limit = page.size;
    }
  }

  const overRecord = (pageNumber - 1) * limit;
  const limitRecord = limit * pageNumber;

  const sortRecord = {};
  if (sort && Object.keys(sort).length !== 0 && sort.constructor === Object) {
    const keys = Object.keys(sort);
    const key = keys[0];
    sortRecord.name = key;
    sortRecord.type = sort[key];
  }

  postModel
    .getPosts(overRecord, limitRecord, sortRecord, filter, keyword)
    .then((posts) => {
      res.json({
        data: posts,
      });
    })
    .catch((err) => {
      if (Object.prototype.hasOwnProperty.call(err, 'code')) {
        if (err.code === errorSql.ER_BAD_FIELD_ERROR) {
          return res.status(400).json({
            errors: [
              {
                status: 400,
                title: messages.en.badRequest,
              },
            ],
          });
        }
      }
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

postRoutes.get('/:slug', (req, res) => {
  const { slug } = req.params;
  Promise.all([
    postModel.getPostBySlug(slug),
    tagModel.getTagsByPostSlug(slug),
    categoryModel.getCategoriesByPostSlug(slug),
  ])
    .then(([post, tags, categories]) => {
      res.json({
        type: typeResponse.POSTS,
        slug,
        attributes: post,
        relationships: {
          tags,
          categories,
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

module.exports = postRoutes;
