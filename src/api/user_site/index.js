const express = require('express');
const categoryRoutes = require('./categories');
const postRoutes = require('./posts');

const router = express.Router();

router.use('/categories', categoryRoutes);
router.use('/posts', postRoutes);

module.exports = router;
