const express = require('express');
const categoryRoutes = express.Router();
const CategoryModel = require('../../../models/category');
const { messages } = require('../../../messages');
const categoryModel = new CategoryModel();

/**
 * [user] Route get all category
 * **/
categoryRoutes.get('/', (req, res) => {
  categoryModel
    .getCategories()
    .then((categories) => {
      res.json({
        data: categories,
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});
module.exports = categoryRoutes;
