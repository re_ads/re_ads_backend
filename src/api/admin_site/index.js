const express = require('express');
const userRoutes = require('./users');
const categoryRoutes = require('./categories');
const postRoutes = require('./posts');
const tagRoutes = require('./tags');

const router = express.Router();

router.use('/users', userRoutes);
router.use('/categories', categoryRoutes);
router.use('/posts', postRoutes);
router.use('/tags', tagRoutes);

module.exports = router;
