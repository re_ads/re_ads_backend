const express = require('express');
const checkParamsIsEmpty = require('../../../utils/checkParamsIsEmpty');
const categoryRoutes = express.Router();
const convertToSlug = require('../../../utils/slug');
const CategoryModel = require('../../../models/category');
const { v4: uuid } = require('uuid');
const { messages } = require('../../../messages');
const errorSql = require('../../../consts/errorSql.consts');
const typeResponse = require('../../../consts/typeResponse.consts');
const categoryModel = new CategoryModel();
/**
 * [admin] Route create category
 * **/
// TODO: unique title category
categoryRoutes.post('/', (req, res) => {
  const { parentId, title, metaTitle, content } = req.body;
  const checked = checkParamsIsEmpty({ title });
  if (checked.isEmpty) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title:
            checked.params.join(',') + ' ' + messages.en.areNullOrUndefined,
        },
      ],
    });
  }
  const slug = convertToSlug(title, false);
  const id = uuid();
  const category = new CategoryModel(
    id,
    title,
    slug,
    parentId,
    metaTitle,
    content
  );

  categoryModel
    .createCategory(category)
    .then(() => {
      res.sendStatus(204);
    })
    .catch((err) => {
      if (err.code === errorSql.ER_DUP_ENTRY) {
        res.status(409).json({
          errors: [
            {
              status: 409,
              title: messages.en.duplicateCategory,
            },
          ],
        });
      } else {
        res.status(500).json({
          errors: [
            {
              status: 500,
              title: messages.en.serverError,
            },
          ],
        });
      }
    });
});

/**
 * [admin] Route update category
 * **/
categoryRoutes.put('/:id', (req, res) => {
  const { id } = req.params;
  const { parentId, title, metaTitle, content } = req.body;
  const slug = convertToSlug(title, false);
  categoryModel
    .updateCategory({ parentId, title, slug, metaTitle, content }, id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.CATEGORIES,
          id,
          attributes: {
            title: '',
            text: messages.en.updateCategorySuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route delete forever category
 * **/
categoryRoutes.delete('/:id', (req, res) => {
  const { id } = req.params;
  categoryModel
    .deleteCategory(id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.CATEGORIES,
          id,
          attributes: {
            title: '',
            text: messages.en.deleteCategorySuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route get all category
 * **/
categoryRoutes.get('/', (req, res) => {
  categoryModel
    .getCategories()
    .then((categories) => {
      res.json({
        data: categories,
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});
module.exports = categoryRoutes;
