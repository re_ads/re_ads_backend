const express = require('express');
const convertToSlug = require('../../../utils/slug');
const tagRoutes = express.Router();
const { v4: uuid } = require('uuid');
const TagModel = require('../../../models/tag');
const { messages } = require('../../../messages');
const tagModel = new TagModel();

tagRoutes.post('/', (req, res) => {
  const { name } = req.body;
  const slug = convertToSlug(name, false);
  const id = uuid();
  const tag = new TagModel(id, name, slug);
  tagModel
    .createTag(tag)
    .then(() => {
      res.sendStatus(204);
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});
module.exports = tagRoutes;
