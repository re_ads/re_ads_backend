const express = require('express');
const validateEmail = require('../../../utils/validateEmail');
const userRoutes = express.Router();
const { messages } = require('../../../messages');
const generatePassword = require('../../../utils/generatePassword');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const { v4: uuid } = require('uuid');
const dateUtils = require('../../../utils/dateUtils');
const UserModel = require('../../../models/user');
const UserRoleModel = require('../../../models/userRole');
const RoleModel = require('../../../models/role');
const checkParamsIsEmpty = require('../../../utils/checkParamsIsEmpty');
const errorSql = require('../../../consts/errorSql.consts');
const validatePhoneNumber = require('../../../utils/validatePhoneNumber');
const userStatus = require('../../../consts/userStatus.consts');
const typeResponse = require('../../../consts/typeResponse.consts');
const pagination = require('../../../consts/pagination.consts');
const generateReminderToken = require('../../../utils/generateReminderToken');

const userModel = new UserModel();
const roleModel = new RoleModel();
const userRoleModel = new UserRoleModel();

// TODO: add middleware
/**
 * [admin] Route create user
 * **/
userRoutes.post('/', async (req, res) => {
  const {
    firstName,
    lastName,
    email,
    mobile,
    intro,
    profile,
    roles,
  } = req.body;
  const checked = checkParamsIsEmpty({
    firstName,
    lastName,
    email,
    mobile,
    roles,
  });
  if (checked.isEmpty) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title:
            checked.params.join(',') + ' ' + messages.en.areNullOrUndefined,
        },
      ],
    });
  }

  const isValidEmail = validateEmail(email);
  if (!isValidEmail) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.inValidEmail,
        },
      ],
    });
  }

  const isValidPhoneNumber = validatePhoneNumber(mobile);
  if (!isValidPhoneNumber) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.inValidPhoneNumber,
        },
      ],
    });
  }

  const userId = uuid();
  const password = await bcrypt.hash(generatePassword(), saltRounds);
  const passwordReminderExpire = dateUtils.formatyyyMMddHHmmss(
    dateUtils.addMinutes(new Date(), 30)
  );
  const passwordReminderToken = generateReminderToken();

  const user = new UserModel(
    userId,
    firstName,
    lastName,
    email,
    mobile,
    password,
    dateUtils.formatyyyMMddHHmmss(new Date()),
    null,
    intro,
    profile,
    passwordReminderToken,
    passwordReminderExpire
  );

  // TODO: send email with reminder token to email

  userModel
    .createUser(user)
    .then(() => {
      return Promise.all(
        roles.map((role) => {
          return roleModel.getRoleByName(role);
        })
      );
    })
    .then((roles) => {
      return Promise.all(
        roles.map((role) => {
          const userRole = new UserRoleModel(userId, role.id);
          userRoleModel.createUserRole(userRole);
        })
      );
    })
    .then(() => {
      res.sendStatus(204);
    })
    .catch((err) => {
      if (err.code === errorSql.ER_DUP_ENTRY) {
        res.status(409).json({
          errors: [
            {
              status: 409,
              title: messages.en.duplicateEmail,
            },
          ],
        });
      } else {
        res.status(500).json({
          errors: [
            {
              status: 500,
              title: messages.en.serverError,
            },
          ],
        });
      }
    });
});

// TODO: add fulltext search
/**
 * [admin] Route get list user
 * **/
userRoutes.get('/', (req, res) => {
  const { page, filter, sort, keyword } = req.query;
  let pageNumber = pagination.PAGE_NUMBER;
  let limit = pagination.LIMIT;

  if (page && Object.keys(page).length !== 0 && page.constructor === Object) {
    if (Object.prototype.hasOwnProperty.call(page, 'number')) {
      pageNumber = page.number;
    }
    if (Object.prototype.hasOwnProperty.call(page, 'size')) {
      limit = page.size;
    }
  }

  const overRecord = (pageNumber - 1) * limit;

  const sortRecord = {};
  if (sort && Object.keys(sort).length !== 0 && sort.constructor === Object) {
    const keys = Object.keys(sort);
    const key = keys[0];
    sortRecord.name = key;
    sortRecord.type = sort[key];
  }

  Promise.all([
    userModel.getUsers(overRecord, limit, sortRecord, filter, keyword),
    userModel.getTotalUser(),
  ])
    .then(([users, total]) => {
      const userRes = users.map((user) => {
        if (user.status === userStatus.ACTIVE) {
          user.statusName = userStatus.ACTIVE_NAME;
        }
        if (user.status === userStatus.UNACTIVE) {
          user.statusName = userStatus.UNACTIVE_NAME;
        }
        if (user.status === userStatus.DELETED) {
          user.statusName = userStatus.DELETED_NAME;
        }

        return user;
      });
      res.json({
        meta: {
          totalPages: Math.ceil(total / limit),
        },
        data: userRes,
        links: {
          self: req.originalUrl,
          first: '',
          prev: '',
          next: '',
          last: '',
        },
      });
    })
    .catch((err) => {
      if (Object.prototype.hasOwnProperty.call(err, 'code')) {
        if (err.code === errorSql.ER_BAD_FIELD_ERROR) {
          return res.status(400).json({
            errors: [
              {
                status: 400,
                title: messages.en.badRequest,
              },
            ],
          });
        }
      }
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route get detail user
 * **/
userRoutes.get('/:id', (req, res) => {
  const { id } = req.params;
  Promise.all([userModel.getUserById(id), roleModel.getRoleByUserId(id)])
    .then(([user, roles]) => {
      if (!user) {
        return res.status(404).json({
          errors: [
            {
              status: 404,
              title: messages.en.userDoesNotExist,
            },
          ],
        });
      }

      if (user.status === userStatus.ACTIVE) {
        user.statusName = userStatus.ACTIVE_NAME;
      }
      if (user.status === userStatus.UNACTIVE) {
        user.statusName = userStatus.UNACTIVE_NAME;
      }
      if (user.status === userStatus.DELETED) {
        user.statusName = userStatus.DELETED_NAME;
      }

      user.roles = roles;

      res.json({
        type: typeResponse.USERS,
        id,
        attributes: user,
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route delete forever user
 * **/
userRoutes.delete('/:id', (req, res) => {
  const { id } = req.params;
  userModel
    .deleteUserById(id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.USERS,
          id,
          attributes: {
            title: '',
            text: messages.en.deleteUserSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route update user
 * **/
// TODO: bổ sung thêm khi hoàn thiện frontend
userRoutes.put('/:id', (req, res) => {
  const { id } = req.params;
  const { firstName, lastName, intro, profile } = req.body;

  userModel
    .updateUser({ firstName, lastName, intro, profile }, id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.USERS,
          id,
          attributes: {
            title: '',
            text: messages.en.updateUserSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route change status user
 * **/
userRoutes.patch('/:id/status', (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  if (!status) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.badRequest,
        },
      ],
    });
  }
  let statusUpdate = userStatus.UNACTIVE;

  if (status === userStatus.ACTIVE_NAME) {
    statusUpdate = userStatus.ACTIVE;
  } else if (status === userStatus.DELETED_NAME) {
    statusUpdate = userStatus.DELETED;
  } else if (status === userStatus.UNACTIVE_NAME) {
    statusUpdate = userStatus.UNACTIVE;
  } else {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.badRequest,
        },
      ],
    });
  }

  userModel
    .updateUser({ status: statusUpdate }, id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.USERS,
          id,
          attributes: {
            title: '',
            text: messages.en.updateUserSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route change roles user
 * **/
userRoutes.patch('/:id/roles', (req, res) => {
  const { id } = req.params;
  const { roles } = req.body;
  if (!roles || (Array.isArray(roles) && !roles.length)) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.badRequest,
        },
      ],
    });
  }

  Promise.all(
    roles.map((role) => {
      return roleModel.getRoleByName(role);
    })
  )
    .then((roles) => {
      return Promise.all([
        userRoleModel.deleteUserRoleByUserId(id),
        ...roles.map((role) => {
          const userRole = new UserRoleModel(id, role.id);
          return userRoleModel.createUserRole(userRole);
        }),
      ]);
    })
    .then(() => {
      res.json({
        data: {
          type: typeResponse.USERS,
          id,
          attributes: {
            title: '',
            text: messages.en.updateUserSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

module.exports = userRoutes;
