const express = require('express');
const postRoutes = express.Router();
const { v4: uuid } = require('uuid');
const convertToSlug = require('../../../utils/slug');
const dateUtils = require('../../../utils/dateUtils');
const authenticateToken = require('../../../middleware/authMiddleware');
const postStatus = require('../../../consts/postStatus.consts');

const PostModel = require('../../../models/post');
const UserModel = require('../../../models/user');
const PostTagModel = require('../../../models/postTag');
const PostCategoryModel = require('../../../models/postCategory');
const TagModel = require('../../../models/tag');
const CategoryModel = require('../../../models/category');
const { messages } = require('../../../messages');
const pagination = require('../../../consts/pagination.consts');
const errorSql = require('../../../consts/errorSql.consts');
const typeResponse = require('../../../consts/typeResponse.consts');
const postModel = new PostModel();
const userModel = new UserModel();
const postTagModel = new PostTagModel();
const postCategoryModel = new PostCategoryModel();
const tagModel = new TagModel();
const categoryModel = new CategoryModel();

postRoutes.post('/', authenticateToken, (req, res) => {
  const { user } = req;
  const {
    title,
    metaTitle,
    summary,
    content,
    mapLatitude,
    mapLongitude,
    tags,
    categoryId,
  } = req.body;
  const postId = uuid();
  const slug = convertToSlug(title);
  userModel
    .getUserInfoByEmail(user.email)
    .then((user) => {
      const authorId = user.id;
      const createdAt = dateUtils.formatyyyMMddHHmmss(new Date());
      const post = new PostModel(
        postId,
        authorId,
        title,
        slug,
        createdAt,
        metaTitle,
        summary,
        content,
        mapLatitude,
        mapLongitude
      );

      console.log(post);

      return postModel.createPost(post);
    })
    .then(() => {
      if (Array.isArray(tags) && tags.length) {
        return Promise.all(
          tags.map((tagId) => {
            const postTag = new PostTagModel(postId, tagId);
            postTagModel.createPostTag(postTag);
          })
        );
      }
    })
    .then(() => {
      const postCategory = new PostCategoryModel(postId, categoryId);
      return postCategoryModel.createPostCategory(postCategory);
    })
    .then(() => {
      res.sendStatus(204);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

postRoutes.get('/', (req, res) => {
  const { page, filter, sort, keyword } = req.query;
  let pageNumber = pagination.PAGE_NUMBER;
  let limit = pagination.LIMIT;

  if (page && Object.keys(page).length !== 0 && page.constructor === Object) {
    if (Object.prototype.hasOwnProperty.call(page, 'number')) {
      pageNumber = page.number;
    }
    if (Object.prototype.hasOwnProperty.call(page, 'size')) {
      limit = page.size;
    }
  }

  const overRecord = (pageNumber - 1) * limit;
  const limitRecord = limit * pageNumber;

  const sortRecord = {};
  if (sort && Object.keys(sort).length !== 0 && sort.constructor === Object) {
    const keys = Object.keys(sort);
    const key = keys[0];
    sortRecord.name = key;
    sortRecord.type = sort[key];
  }

  postModel
    .getPosts(overRecord, limitRecord, sortRecord, filter, keyword)
    .then((posts) => {
      res.json({
        data: posts,
      });
    })
    .catch((err) => {
      if (Object.prototype.hasOwnProperty.call(err, 'code')) {
        if (err.code === errorSql.ER_BAD_FIELD_ERROR) {
          return res.status(400).json({
            errors: [
              {
                status: 400,
                title: messages.en.badRequest,
              },
            ],
          });
        }
      }
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

postRoutes.get('/:slug', (req, res) => {
  const { slug } = req.params;
  Promise.all([
    postModel.getPostBySlug(slug),
    tagModel.getTagsByPostSlug(slug),
    categoryModel.getCategoriesByPostSlug(slug),
  ])
    .then(([post, tags, categories]) => {
      res.json({
        type: typeResponse.POSTS,
        slug,
        attributes: post,
        relationships: {
          tags,
          categories,
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * [admin] Route change status post
 * **/
postRoutes.patch('/:id/status', (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  if (!status) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.badRequest,
        },
      ],
    });
  }
  let statusUpdate = postStatus.UNACTIVE;

  if (status === postStatus.ACTIVE_NAME) {
    statusUpdate = postStatus.ACTIVE;
  } else if (status === postStatus.DELETED_NAME) {
    statusUpdate = postStatus.DELETED;
  } else if (status === postStatus.UNACTIVE_NAME) {
    statusUpdate = postStatus.UNACTIVE;
  } else {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title: messages.en.badRequest,
        },
      ],
    });
  }
  postModel
    .updatePost({ status: statusUpdate }, id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.POSTS,
          id,
          attributes: {
            title: '',
            text: messages.en.updatePostSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

postRoutes.delete('/:id', (req, res) => {
  const { id } = req.params;
  postModel
    .deletePostById(id)
    .then(() => {
      res.json({
        data: {
          type: typeResponse.POSTS,
          id,
          attributes: {
            title: '',
            text: messages.en.deletePostSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

postRoutes.put('/:id', (req, res) => {
  const { id } = req.params;
  const {
    title,
    metaTitle,
    summary,
    content,
    mapLatitude,
    mapLongitude,
    tags,
    categoryId,
  } = req.body;

  const slug = convertToSlug(title);

  Promise.all([
    postModel.updatePost(
      {
        title,
        metaTitle,
        summary,
        content,
        mapLatitude,
        mapLongitude,
        slug,
      },
      id
    ),
    postTagModel.deletePostTagByPostId(id),
    postCategoryModel.deletePostCategoryByPostId(id),
    ...tags.map((tag) => {
      const postTag = new PostTagModel(id, tag);
      postTagModel.createPostTag(postTag);
    }),
    postCategoryModel.createPostCategory(new PostCategoryModel(id, categoryId)),
  ])
    .then(() => {
      res.json({
        data: {
          type: typeResponse.POSTS,
          id,
          attributes: {
            title: '',
            text: messages.en.updatePostSuccess,
          },
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});
module.exports = postRoutes;
