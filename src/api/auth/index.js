const express = require('express');
const authRoutes = express.Router();
const jwt = require('jsonwebtoken');
const UserModel = require('../../models/user');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const { messages } = require('../../messages');
const authenticateToken = require('../../middleware/authMiddleware');
const checkParamsIsEmpty = require('../../utils/checkParamsIsEmpty');
const validatePassword = require('../../utils/validatePassword');
const validateEmail = require('../../utils/validateEmail');
const dateUtils = require('../../utils/dateUtils');
const tokenConsts = require('../../consts/token.consts');
const generateReminderToken = require('../../utils/generateReminderToken');
const compareTimeConsts = require('../../consts/compareTime.consts');

const userModel = new UserModel();
let refreshTokens = [];

/**
 * Route login
 * **/
authRoutes.post('/login', async (req, res) => {
  const { email, password } = req.body;

  userModel
    .getUserInfoByEmail(email)
    .then(async (user) => {
      if (!user) {
        return res.status(401).json({
          errors: [
            {
              status: 401,
              title: messages.en.userDoesNotExist,
            },
          ],
        });
      }
      const match = await bcrypt.compare(password, user.password);
      if (!match) {
        return res.status(401).json({
          errors: [
            {
              status: 401,
              title: messages.en.incorrectPassword,
            },
          ],
        });
      }

      const accessToken = generateAccessToken({ email: user.email });
      const refreshToken = generateRefreshToken({ email: user.email });

      refreshTokens.push(refreshToken);

      res.json({
        data: {
          accessToken,
          refreshToken,
        },
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

/**
 * Route refresh token
 * **/
authRoutes.post('/token', (req, res) => {
  const { refreshToken } = req.body;
  if (refreshToken === null) {
    return res.status(401).json({
      errors: [
        {
          status: 401,
          title: 'refreshToken ' + messages.en.isNull,
        },
      ],
    });
  }

  if (!refreshTokens.includes(refreshToken)) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) {
      return res.status(403).json({
        errors: [
          {
            status: 403,
            title: messages.en.forbiddenError,
          },
        ],
      });
    }
    const accessToken = generateAccessToken({ email: user.email });
    res.json({
      data: {
        accessToken,
      },
    });
  });
});

/**
 * Route logout
 * **/
authRoutes.delete('/logout', (req, res) => {
  try {
    const { refreshToken } = req.body;
    if (!refreshToken) {
      return res.status(401).json({
        errors: [
          {
            status: 401,
            title: 'refreshToken ' + messages.en.isNullOrUndefined,
          },
        ],
      });
    }
    refreshTokens = refreshTokens.filter((token) => token !== refreshToken);
    res.sendStatus(204);
  } catch (err) {
    res.status(500).json({
      errors: [
        {
          status: 500,
          title: messages.en.serverError,
        },
      ],
    });
  }
});

/**
 * Route get info user login
 * **/
authRoutes.get('/me', authenticateToken, (req, res) => {
  const { user } = req;
  userModel
    .getUserByEmail(user.email)
    .then((user) => {
      res.json({
        data: user,
      });
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

authRoutes.patch('/:id/password', authenticateToken, (req, res) => {
  const { oldPassword, newPassword, confirmPassword } = req.body;
  const { user } = req;
  const checked = checkParamsIsEmpty({
    oldPassword,
    newPassword,
    confirmPassword,
  });
  if (checked.isEmpty) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title:
            checked.params.join(',') + ' ' + messages.en.areNullOrUndefined,
        },
      ],
    });
  }
  if (oldPassword === newPassword) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }
  if (newPassword !== confirmPassword) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }

  const validPassword = validatePassword(newPassword);

  if (!validPassword) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }

  userModel
    .getUserInfoByEmail(user.email)
    .then(async (user) => {
      const match = await bcrypt.compare(newPassword, user.password);

      if (match) {
        return res.status(403).json({
          errors: [
            {
              status: 403,
              title: messages.en.forbiddenError,
            },
          ],
        });
      }

      const passwordHash = bcrypt.hashSync(newPassword, saltRounds);
      return userModel.updateUser({ password: passwordHash }, user.id);
    })
    .then(() => {
      res.sendStatus(204);
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

authRoutes.post('/password/forgot', (req, res) => {
  const { email } = req.body;
  const checked = checkParamsIsEmpty({ email });
  if (checked.isEmpty) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title:
            checked.params.join(',') + ' ' + messages.en.areNullOrUndefined,
        },
      ],
    });
  }
  const validEmail = validateEmail(email);
  if (!validEmail) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }

  const passwordReminderToken = generateReminderToken();

  const passwordReminderExpire = dateUtils.formatyyyMMddHHmmss(
    dateUtils.addMinutes(new Date(), 30)
  );

  userModel
    .getUserInfoByEmail(email)
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          errors: [
            {
              status: 401,
              title: messages.en.userDoesNotExist,
            },
          ],
        });
      }

      return userModel.updateUser(
        { passwordReminderToken, passwordReminderExpire },
        user.id
      );
    })
    .then(() => {
      // TODO: send link to email
      res.sendStatus(204);
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

authRoutes.get('password/reset/:token', (req, res) => {
  const { token } = req.params;
  userModel
    .getUserByPasswordReminderToken(token)
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          errors: [
            {
              status: 401,
              title: 'refreshToken ' + messages.en.isNullOrUndefined,
            },
          ],
        });
      }
      const match = token === user.passwordReminderToken;

      if (!match) {
        return res.status(403).json({
          errors: [
            {
              status: 403,
              title: messages.en.forbiddenError,
            },
          ],
        });
      }

      const resultCompareExpire = dateUtils.compareTime(
        Date.now(),
        user.passwordReminderExpire
      );
      if (
        resultCompareExpire === compareTimeConsts.GREATER ||
        resultCompareExpire === null
      ) {
        return res.status(403).json({
          errors: [
            {
              status: 403,
              title: messages.en.forbiddenError,
            },
          ],
        });
      }

      res.sendStatus(204);
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

authRoutes.post('/password/reset', async (req, res) => {
  const { newPassword, confirmPassword } = req.body;
  const checked = checkParamsIsEmpty({
    token,
    newPassword,
    confirmPassword,
  });
  if (checked.isEmpty) {
    return res.status(400).json({
      errors: [
        {
          status: 400,
          title:
            checked.params.join(',') + ' ' + messages.en.areNullOrUndefined,
        },
      ],
    });
  }

  if (newPassword !== confirmPassword) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }

  const validPassword = validatePassword(newPassword);

  if (!validPassword) {
    return res.status(403).json({
      errors: [
        {
          status: 403,
          title: messages.en.forbiddenError,
        },
      ],
    });
  }

  const password = await bcrypt.hash(newPassword, saltRounds);

  userModel
    .getUserByPasswordReminderToken(token)
    .then((user) => {
      if (!user) {
        return res.status(401).json({
          errors: [
            {
              status: 401,
              title: 'refreshToken ' + messages.en.isNullOrUndefined,
            },
          ],
        });
      }

      return userModel.updateUser(
        { password, passwordReminderToken: null, passwordReminderExpire: null },
        user.id
      );
    })
    .then(() => {
      res.sendStatus(204);
    })
    .catch((err) => {
      res.status(500).json({
        errors: [
          {
            status: 500,
            title: messages.en.serverError,
          },
        ],
      });
    });
});

function generateAccessToken(user) {
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: tokenConsts.EXPIRE_ACCESS_TOKEN,
  });
}

function generateRefreshToken(user) {
  return jwt.sign(
    user,
    process.env.REFRESH_TOKEN_SECRET,
    tokenConsts.EXPIRE_REFRESH_TOKEN
  );
}

module.exports = authRoutes;
